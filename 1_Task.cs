using System;
using System.Collections.Generic;


namespace ConsoleApplication1
{
    class Program
    {
        class Rectangle 
        {
            private double CoordinateX1;
            private double CoordinateY1;
            private double CoordinateX2;
            private double CoordinateY2;
            private double CoordinateX3;
            private double CoordinateY3;
            private double CoordinateX4;
            private double CoordinateY4;

            public Rectangle()
            {

            }

            public Rectangle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4)
            {
                this.CoordinateX1 = x1;
                this.CoordinateY1 = y1;
                this.CoordinateX2 = x2;
                this.CoordinateY2 = y2;
                this.CoordinateX3 = x3;
                this.CoordinateY3 = y3;
                this.CoordinateX4 = x4;
                this.CoordinateY4 = y4;
            }

            #region Properties
            public double x1
            {
                get { return CoordinateX1; }
                set { CoordinateX1 = value; }
            }

            public double y1
            {
                get { return CoordinateY1; }
                set { CoordinateY1 = value; }
            }

            public double x2
            {
                get { return CoordinateX2; }
                set { CoordinateX2 = value; }
            }

            public double y2
            {
                get { return CoordinateY2; }
                set { CoordinateY2 = value; }
            }
            public double x3
            {
                get { return CoordinateX3; }
                set { CoordinateX3 = value; }
            }

            public double y3
            {
                get { return CoordinateY3; }
                set { CoordinateY3 = value; }
            }
            public double x4
            {
                get { return CoordinateX4; }
                set { CoordinateX4 = value; }
            }

            public double y4
            {
                get { return CoordinateY4; }
                set { CoordinateY4 = value; }
            }
            #endregion

           
            public override string ToString()
            {
                return "Rectangle has next coordinats : (" + x1 + "," + y1 + ") (" + x2 + "," + y2 + ") (" + x3 + "," + y3 + ") (" + x4 + "," + y4 + ")";
            }

            static public Rectangle ExternalRectangle(Rectangle x, Rectangle y)
            {
                Rectangle External = new Rectangle();        
  
                if(Math.Sqrt(Math.Pow((y.x3 - x.x1),2) + Math.Pow((y.y3 - x.y1),2)) >
                    Math.Sqrt(Math.Pow((x.x3 - y.x1), 2) + Math.Pow((x.y3 - y.y1), 2)))
                {
                    External.x1 = x.x1;
                    External.y1 = x.y1;

                    External.x2 = x.x1;
                    External.y2 = y.y3;

                    External.x3 = y.x3;
                    External.y3 = y.y3;

                    External.x4 = y.x3;
                    External.y4 = x.y1;
                }
                else
                {
                    External.x1 = y.x1;
                    External.y1 = y.y1;

                    External.x2 = y.x1;
                    External.y2 = x.y3;

                    External.x3 = x.x3;
                    External.y3 = x.y3;

                    External.x4 = x.x3;
                    External.y4 = y.y1;
                }

                return External;
            }

            static public Rectangle CrossingRectangle(Rectangle x, Rectangle y)
            {
                Rectangle Crossing = new Rectangle();
                if (Math.Sqrt(Math.Pow((y.x3 - x.x1), 2) + Math.Pow((y.y3 - x.y1), 2)) >
                   Math.Sqrt(Math.Pow((x.x3 - y.x1), 2) + Math.Pow((x.y3 - y.y1), 2)))
                {
                    Crossing.x1 = y.x1;
                    Crossing.y1 = y.y4;

                    Crossing.x2 = y.x1;
                    Crossing.y2 = x.y3;

                    Crossing.x3 = x.x4;
                    Crossing.y3 = x.y2;

                    Crossing.x4 = x.x4;
                    Crossing.y4 = y.y4;
                }
               else
                {
                    Crossing.x1 = x.x1;
                    Crossing.y1 = x.y4;

                    Crossing.x2 = x.x1;
                    Crossing.y2 = y.y3;

                    Crossing.x3 = y.x4;
                    Crossing.y3 = y.y2;

                    Crossing.x4 = y.x4;
                    Crossing.y4 = x.y4;
                }

                return Crossing;
            }
        }
        static void Main(string[] args)
        {
            List<Rectangle> ListOfRectangles = new List<Rectangle>();

            //External rectangle
            Rectangle myRectangle = new Rectangle(1,1, 1,3, 3,3, 3,1);
            ListOfRectangles.Add(myRectangle);
            Rectangle myRect = new Rectangle(5,5, 5,7, 7,7, 7,5);
            ListOfRectangles.Add(myRect);        
            ListOfRectangles.Add(Rectangle.ExternalRectangle(myRectangle, myRect));
           
            //External rectangle with negative coordinates
            Rectangle myrect1 = new Rectangle(-3,-2, -3,-1, -1,-1, -1,-2);
            Rectangle myrect2 = new Rectangle(-6,-4, -6,-2, -4,-2, -4,-4);
            ListOfRectangles.Add(Rectangle.ExternalRectangle(myrect2, myrect1));
           
            //Crossing rectangle 
            Rectangle myrect3 = new Rectangle(2,2, 2,5, 8,5, 8,2);
            Rectangle myrect4 = new Rectangle(6,4, 6,6, 9,6, 9,4);
            ListOfRectangles.Add(Rectangle.CrossingRectangle(myrect4, myrect3));
           
            //Crossing rectangle 
            Rectangle myrect5 = new Rectangle(-2,-2, -2,-5, -8,-5, -8,-2);
            Rectangle myrect6 = new Rectangle(-3,-3, -3,-1, -1,-1, -1,-3);
            ListOfRectangles.Add(Rectangle.CrossingRectangle(myrect6, myrect5));
           
            ////External rectagle
            //Rectangle myrect7 = new Rectangle(2,2, 2,5, 8,5, 8,2);
            //Rectangle myrect8 = new Rectangle(6,4, 6,6, 9,6, 9,4);
            //ListOfRectangles.Add(Rectangle.ExternalRectangle(myrect7, myrect8));

            foreach (Rectangle x in ListOfRectangles)
            {
               Console.WriteLine(x.ToString());
            }
        }
    }
}
