using System;

namespace ConsoleApplication2
{
    class Program
    {

        public class LimitationException : Exception
        {
            public LimitationException()
            {
                Console.WriteLine("Limitation error.");
            }
        }
            class TernaryVector
        {
            public int[] Values;
            public int[] Limitation = { 0, 1, 2 };
            public TernaryVector()
            {

            }

            public TernaryVector(params int[] array)
            {
                foreach(int x in array)
                {
                    if (x != 0 && x != 1 && x != 2) throw new LimitationException();
                }
                Values = array;
            }

            static public bool Orthogonality(TernaryVector a, TernaryVector b)
            {
                int Sum1 = 1;
                int Sum2 = 1;

                for (int i = 0; i < a.Values.Length; i++)
                    Sum1 *= a.Values[i];

                for (int i = 0; i < b.Values.Length; i++)
                    Sum2 *= b.Values[i];


                if(Sum1+Sum2 == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                } 
            }

            static public int CountOfComponents2(TernaryVector a)
            {
                int count = 0;

                for(int i=0; i<a.Values.Length; i++)
                {
                    if (a.Values[i] == 2) count++;
                }

                return count;
            }

            static public TernaryVector Crossing(TernaryVector a, TernaryVector b)
            {
                TernaryVector crossed = new TernaryVector();
                crossed.Values = a.Values;

                //TernaryVector crossed = new TernaryVector(a.Values.Length);

                if(TernaryVector.Orthogonality(a,b) == false)
                {
                    if(a.Values.Length==b.Values.Length)
                    {
                        for(int i=0; i<a.Values.Length; i++)
                        {
                            if (a.Values[i] == 1 && b.Values[i] == 1) crossed.Values[i] = 1;
                            if ((a.Values[i] == 1 && b.Values[i] == 2) || (a.Values[i] == 2 && b.Values[i] == 1))
                                crossed.Values[i] = 1;
                            
                            if (a.Values[i] == 0 && b.Values[i] == 0)
                                crossed.Values[i] = 0;
                            if ((a.Values[i] == 0 && b.Values[i] == 2) || (a.Values[i] == 2 && b.Values[i] == 0))
                                crossed.Values[i] = 0;
                            if ((a.Values[i] == 1 && b.Values[i] == 0) || (a.Values[i] == 0 && b.Values[i] == 1))
                                crossed.Values[i] = 0;                      
                            
                            if (a.Values[i] == 2 && b.Values[i] == 2) crossed.Values[i] = 2;
                        }
                    }
                    else
                    {
                        throw new Exception("Vectors are different lengths.");
                    }
                }
                else
                {
                    throw new Exception("Vectors is ortogonal.");
                }
                return crossed;
            }
            
            public override string ToString()
            {
                string s= "";
                foreach(int x in Values)
                {
                    s += x + " ";
                }
                return s;
            }
        }
        static void Main(string[] args)
        {
            try
            {
                TernaryVector myVector1 = new TernaryVector(2,1,2);
                TernaryVector myVector2 = new TernaryVector(1,2,1);

                Console.Write("Is MyVector1 orthogonal to MyVector2? : ");
                Console.WriteLine(TernaryVector.Orthogonality(myVector1, myVector2));

                Console.Write("myVector1 : ");
                Console.WriteLine(myVector1.ToString());
                Console.Write("myVector2 : ");
                Console.WriteLine(myVector2.ToString());

                Console.Write("Count of components 2 in MyVector1 : ");
                Console.WriteLine(TernaryVector.CountOfComponents2(myVector1));
   
                Console.Write("Crossing myVector1 and myVector2 : ");
                Console.WriteLine(TernaryVector.Crossing(myVector1, myVector2));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
